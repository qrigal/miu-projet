# Projet MIU
Quentin Rigal - Maxime Cassina - Thibault Béteille

-------

`MIU documentation.pdf` -> Diagrammes de l'application

`Maquette` -> Maquette sous forme HTML avec liens entre les écrans.
>Pour visualiser la maquette, vous devez cloner le repository ou le télécharger en .zip \
puis ouvrir `index.html` avec votre navigateur.